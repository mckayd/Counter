package ie.tcd.scss.counter;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class select extends AppCompatActivity {

    public final static String LABELS = "ie.tcd.scss.counter.LABELS";

    EditText m1,m2,m3,m4;



    String[] metrics = new String[4];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);

        m1 = (EditText) findViewById(R.id.m1);
        m2 = (EditText) findViewById(R.id.m2);
        m3 = (EditText) findViewById(R.id.m3);
        m4 = (EditText) findViewById(R.id.m4);

        Button b1 = (Button) findViewById(R.id.goButton);

        b1.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  metrics[0] = m1.getText().toString();
                  metrics[1] = m2.getText().toString();
                  metrics[2] = m3.getText().toString();
                  metrics[3] = m4.getText().toString();

                  counter();

              }
          });





        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


    }

    public void counter(){
        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra(LABELS,metrics);
        startActivity(intent);
    }

}
