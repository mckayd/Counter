package ie.tcd.scss.counter;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class SessionListActivity extends AppCompatActivity  {

    JSONObject savedSessions;
    ListView lv;

    public final String FILENAME = "mySession";
    ArrayList<HashMap<String, String>> sessionList = new ArrayList<HashMap<String, String>>();

    public final static String RAW_COUNTS = "ie.tcd.scss.counter.RAW_COUNTS";
    public final static String LABELS = "ie.tcd.scss.counter.LABELS";

    private static final String TAG_NAME = "title";
    private static final String TAG_DATE = "date";
    private static final String TAG_ID = "number";
    private static final String TAG_TS = "timestamp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sessionList = new ArrayList<HashMap<String, String>>();
        lv = (ListView) findViewById(R.id.sessionList);

        try {
            savedSessions =  new JSONObject(readSessions());

            Iterator<?> keys = savedSessions.keys();
            int i=0;
            while( keys.hasNext() ) {
                String time = (String)keys.next();
                HashMap<String, String> map = new HashMap<String, String>();
                if ( savedSessions.get(time) instanceof JSONObject ) {
                    i++;
                    Object sesh_x = savedSessions.get(time);

                    long ts = Integer.parseInt(time.toString());
                    ts = ts*1000;
                    Date date = new Date(ts);
                    Calendar c = Calendar.getInstance();
                    c.setTime(date);
                    //String df =  date.getDay() + "/" + date.getMonth() + "/" + date.getYear();
                    SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yy H:m");
                    String f_time = format1.format(c.getTime());
                    map.put(TAG_DATE,f_time);
                    map.put(TAG_ID,Integer.toString(i));
                    map.put(TAG_TS, time.toString());
                }
                sessionList.add(map);
            }

            ListAdapter adapter = new SimpleAdapter(SessionListActivity.this, sessionList,
                    R.layout.session_list_template,
                    new String[] { TAG_DATE,TAG_ID, TAG_TS }, new int[] {
                    R.id.date,R.id.number,R.id.timestamp});

            lv.setAdapter(adapter);

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    TextView date_text = (TextView) view.findViewById(R.id.date);
                    TextView ts_text = (TextView) view.findViewById(R.id.timestamp);
                    String date_tv = (String) date_text.getText().toString();
                    Toast.makeText(SessionListActivity.this, "You Clicked at " + date_tv, Toast.LENGTH_SHORT).show();

                    int[] counts = new int[]{0, 0, 0, 0,};
                    String[] metrics = new String[]{"", "", "", ""};

                    try {

                        String sesh_x = savedSessions.getString(ts_text.getText().toString());

                        JSONObject sesh_j = new JSONObject(sesh_x);
                        Log.d("Object", sesh_j.toString());
                        Iterator<String> keysIterator = sesh_j.keys();
                        int i = 0;
                        while (keysIterator.hasNext())
                        {
                            String keyStr = (String)keysIterator.next();
                            String valueStr = sesh_j.getString(keyStr);
                            metrics[i] = keyStr;
                            counts[i] = Integer.parseInt(valueStr);
                            i++;
                        }
                        graphSession(counts,metrics);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                    //


                }
            });

        } catch (JSONException e) {
            savedSessions =  new JSONObject();
            e.printStackTrace();
        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                newSession();
            }
        });
    }

    private void graphSession(int[] counts, String[] metrics){
        Intent intent = new Intent(this,GraphActivity.class);
        intent.putExtra(RAW_COUNTS,counts);
        intent.putExtra(LABELS, metrics);
        startActivity(intent);
    }
    private void newSession(){
        Intent intent = new Intent(this,select.class);
        startActivity(intent);
    }

    private String readSessions() {

        String ret = "";

        try {
            InputStream inputStream = openFileInput(FILENAME);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("main activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("main activity", "Can not read file: " + e.toString());
        }

        return ret;
    }




}
