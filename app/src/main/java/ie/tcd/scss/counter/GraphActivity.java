package ie.tcd.scss.counter;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class GraphActivity extends AppCompatActivity {

    int[] counts;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grapher);

        Intent intent = getIntent();
        counts = intent.getIntArrayExtra(MainActivity.RAW_COUNTS);

        BarChart chart = (BarChart) findViewById(R.id.chart);

        String[] labels;

        labels = intent.getStringArrayExtra(MainActivity.LABELS);

        //new ArrayList<Element>(Arrays.asList(array))
        ArrayList<String> xvals = new ArrayList<String>();
        for(int i = 0; i< 4; i++)
        {
            xvals.add(labels[i]);
        }

        ArrayList<BarEntry> yvals = new ArrayList<BarEntry>();
        for(int i = 0; i< 4; i++)
        {
            yvals.add(new BarEntry(counts[i], i));
        }

        BarDataSet set1 = new BarDataSet(yvals, "Session 1");
        set1.setBarSpacePercent(35f);

        ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
        dataSets.add(set1);


        BarData data = new BarData(xvals, dataSets);

        chart.setData(data);


        ///
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

}
