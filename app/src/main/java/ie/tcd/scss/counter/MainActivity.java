package ie.tcd.scss.counter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity {

    public final static String RAW_COUNTS = "ie.tcd.scss.counter.RAW_COUNTS";
    public final static String LABELS = "ie.tcd.scss.counter.LABELS";
    public final static String NAME = "ie.tcd.scss.counter.NAME";

    public final String FILENAME = "mySession_v2";


    Button b1, b2, b3, b4;
    TextView c1,c2,c3,c4;
    int[] counts = new int[]{0,0,0,0,};
    String[] metrics;
    String session_name;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        metrics = intent.getStringArrayExtra(select.LABELS);
        //session_name = intent.getStringExtra(select.NAME);

        b1 =  (Button) findViewById(R.id.button);
        b2 =  (Button) findViewById(R.id.button2);
        b3 =  (Button) findViewById(R.id.button3);
        b4 =  (Button) findViewById(R.id.button4);

        c1 = (TextView) findViewById(R.id.counter1);
        c2 = (TextView) findViewById(R.id.counter2);
        c3 = (TextView) findViewById(R.id.counter3);
        c4 = (TextView) findViewById(R.id.counter4);

        b1.setText(metrics[0]);
        b2.setText(metrics[1]);
        b3.setText(metrics[2]);
        b4.setText(metrics[3]);

        TextView l1 = (TextView) findViewById(R.id.label1);
        TextView l2 = (TextView) findViewById(R.id.label2);
        TextView l3 = (TextView) findViewById(R.id.label3);
        TextView l4 = (TextView) findViewById(R.id.label4);

        l1.setText(metrics[0]);
        l2.setText(metrics[1]);
        l3.setText(metrics[2]);
        l4.setText(metrics[3]);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button saveButton = (Button) findViewById(R.id.saveSession);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    saveSession();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });

        Button makeGraph = (Button) findViewById(R.id.makeGraph);
        makeGraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
           graph();
            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counts[0]++;
                c1.setText(Integer.toString(counts[0]));
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counts[1]++;
                c2.setText(Integer.toString(counts[1]));
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counts[2]++;
                c3.setText(Integer.toString(counts[2]));
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counts[3]++;
                c4.setText(Integer.toString(counts[3]));
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                share();
            }
        });
    }


    public void graph(){
            Intent intent = new Intent(this,GraphActivity.class);
            intent.putExtra(RAW_COUNTS,counts);
            intent.putExtra(LABELS, metrics);
            startActivity(intent);
        }

    public void share(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Come join me on Counter to track your sports stats, (not yet) available on Google Play store!");
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share to.."));
    };

    public void reset(View view){
        for(int i = 0; i<4; i++){
            counts[i] = 0;
        }
        c1.setText(Integer.toString(counts[0]));
        c2.setText(Integer.toString(counts[1]));
        c3.setText(Integer.toString(counts[2]));
        c4.setText(Integer.toString(counts[3]));
    }

    public void saveSession()  throws IOException{

        Long tsLong = System.currentTimeMillis()/1000;
        String time = tsLong.toString();

        //create an empty json object

        JSONObject sesh = new JSONObject();
        try{
            //put each metric and their 'tag' into the object
            for(int i = 0; i< metrics.length; i++){
                sesh.put(metrics[i], counts[i]);
            }

        }catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //read the saved sesssion from the file
        String sessions = readSessions();

        JSONObject sessionsObject = null;
        JSONObject newSessionJSON = null;
       try {
           if(sessions != ""){  //if the file is not blank


               newSessionJSON = new  JSONObject();
               sessionsObject = new JSONObject(sessions);
               Iterator<String> keysIterator = sessionsObject.keys();
               boolean found = false;
               while (keysIterator.hasNext())
               {

                   String keyStr = (String)keysIterator.next();
                   if(keyStr != session_name)
                   {
                       //just put the object back into our bulk object
                       JSONObject sesh_y = new JSONObject(keyStr);
                       newSessionJSON.put(keyStr, sesh_y);
                   }else{
                       //this is the session we're edditing
                        JSONObject sesh_y = new JSONObject(keyStr);
                        sesh_y.put(time,sesh);
                       newSessionJSON.put(keyStr, sesh_y);

                       //mark as found
                       found = true;
                   }

                   if(!found){
                       JSONObject sesh_y = new JSONObject();
                       sesh_y.put(time,sesh);
                       newSessionJSON.put(session_name,sesh_y);
                   }
               }


               //if not create the new Session Name
           }else {
               //use the one we have
               JSONObject sesh_x = new JSONObject();
               JSONObject sesh_y = new JSONObject();
               sesh_y.put(time,sesh);
               sesh_x.put(session_name,sesh_y);
               newSessionJSON.put(session_name,sesh_x);
           }

       }catch (JSONException e){
           // TODO Auto-generated catch block
           e.printStackTrace();
       }
        //convert the object to a string
        sessions = newSessionJSON.toString();
        Log.d("Sessions", sessions);
        //write the object to the file
        writeToFile(sessions);
    }
    private void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput(FILENAME, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
    private String readSessions() {

        String ret = "";

        try {
            InputStream inputStream = openFileInput(FILENAME);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("main activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("main activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

